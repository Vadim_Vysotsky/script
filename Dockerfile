FROM alpine
RUN apk add --no-cache --upgrade bash

ARG JMETER_VERSION="5.5"

ENV JMETER_HOME /app/apache-jmeter-${JMETER_VERSION}
ENV JMETER_BIN  ${JMETER_HOME}/bin
ENV MIRROR_HOST https://archive.apache.org/dist/jmeter
ENV JMETER_DOWNLOAD_URL ${MIRROR_HOST}/binaries/apache-jmeter-${JMETER_VERSION}.tgz
ENV JMETER_PLUGINS_DOWNLOAD_URL https://repo1.maven.org/maven2/kg/apc
ENV JMETER_PLUGINS_FOLDER ${JMETER_HOME}/lib/ext

ENV TIMEZONE Europe/Minsk

RUN    apk update \
	&& apk upgrade \
	&& apk add ca-certificates \
	&& update-ca-certificates \
            && apk add --update openjdk8-jre tzdata curl unzip bash dos2unix \
            && cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
            && echo ${TIMEZONE} >  /etc/timezone \
	&& rm -rf /var/cache/apk/* \
	&& mkdir -p /tmp/dependencies  \
	&& curl -L ${JMETER_DOWNLOAD_URL} -o /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz  \
	&& mkdir -p /app  \
	&& mkdir -p /opt  \
	&& tar -zxvf /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz -C /app  \
	&& curl -L ${JMETER_PLUGINS_DOWNLOAD_URL}/cmdrunner/2.3/cmdrunner-2.3.jar -o ${JMETER_HOME}/lib/cmdrunner-2.3.jar \
	&& curl -L ${JMETER_PLUGINS_DOWNLOAD_URL}/jmeter-plugins-manager/1.8/jmeter-plugins-manager-1.8.jar -o ${JMETER_PLUGINS_FOLDER}/jmeter-plugins-manager-1.8.jar  \
	&& rm -rf /tmp/dependencies

ENV PATH $PATH:$JMETER_BIN

WORKDIR ${JMETER_BIN}

COPY ./launch.sh /
RUN chmod +x /launch.sh

RUN dos2unix /launch.sh

ENTRYPOINT ["sh","/launch.sh"]