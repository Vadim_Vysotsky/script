IMAGE_NAME=jmeter_test
VERSION=latest

.PHONY: build
build:
	@docker build -t $(IMAGE_NAME):$(VERSION) .
