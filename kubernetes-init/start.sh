#!/bin/bash
set -x

SCRIPT_NAME=$1
VUSERS=$2
RAMP_UP=$3
DURATION=$4
THINK_TIME=$5
THINK_TIME_DEVIATION=$6
WORKSPACE=$7

CONFIGPATH=$WORKSPACE/kubernetes-init/

sed -i -e 's/y_SCRIPT_NAME/'$SCRIPT_NAME'/g' $CONFIGPATH'jmeter.yaml'
sed -i -e 's/y_VUSERS/'$VUSERS'/g' $CONFIGPATH'jmeter.yaml'
sed -i -e 's/y_RAMP_UP/'$RAMP_UP'/g' $CONFIGPATH'jmeter.yaml'
sed -i -e 's/y_DURATION/'$DURATION'/g' $CONFIGPATH'jmeter.yaml'
sed -i -e 's/y_THINK_TIME/'$THINK_TIME'/g' $CONFIGPATH'jmeter.yaml'
sed -i -e 's/y_THINK_TIME_DEVIATION/'$THINK_TIME_DEVIATION'/g' $CONFIGPATH'jmeter.yaml'

sed -i -e 's/y_WORKSPACE/'$WORKSPACE'/g' $CONFIGPATH'jmeter.yaml'