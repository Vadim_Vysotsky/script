#!/bin/bash
set -x

JMETER_VERSION="5.5"

export JVM_ARGS="-Xms1g -Xmx1g -XX:MaxMetaspaceSize=256m"

JMETER_HOME="/app/apache-jmeter-$JMETER_VERSION"
echo "JVM_ARGS=${JVM_ARGS}"

echo server.rmi.ssl.disable=true >> ${JMETER_HOME}/bin/user.properties

echo "START Running Jmeter on `date`"

jmeter $@

echo "END Running Jmeter on `date`"